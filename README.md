# fuzzy-commands

A library for building interfaces using fuzzy matching.

Example usage can be found here: https://gitlab.com/TheZoq2/Flash-Frontend/-/blob/master/src/Commands.elm and https://gitlab.com/TheZoq2/Flash-Frontend/-/blob/master/src/MsgCommand.elm

